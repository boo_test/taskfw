#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <taskfw.h>

namespace {
class SimpleTask : public taskfw::TaskWithResult<taskfw::DispatcherID>
{
  void operator()() override
  {
    taskfw_task
    {
      taskfw_success_with_result(taskfw_dispatcher);
    }
  }
};

}  // namespace

TEST(DispatchTest, Dispatch)
{
  auto dispatcher1 = std::make_shared<taskfw::ManualTaskDispatcher>();
  taskfw::Dispatcher::instance().AddTaskDispatcher(taskfw::kMainDispatcherID, dispatcher1);

  auto result = taskfw::dispatch<SimpleTask>();

  while (!result->IsFinished())
    dispatcher1->Process();

  EXPECT_TRUE(result->IsSuccessful());
  EXPECT_EQ(result->GetResult(), taskfw::kMainDispatcherID);
}

TEST(DispatchTest, Dispatch_to)
{
  auto dispatcher1 = std::make_shared<taskfw::ManualTaskDispatcher>();
  auto dispatcher2 = std::make_shared<taskfw::ThreadTaskDispatcher>(std::chrono::microseconds(0));

  taskfw::Dispatcher::instance().AddTaskDispatcher(taskfw::kMainDispatcherID, dispatcher1);
  taskfw::Dispatcher::instance().AddTaskDispatcher(taskfw::kMainDispatcherID + 1, dispatcher2);

  auto result = taskfw::dispatch_to<SimpleTask, taskfw::kMainDispatcherID + 1>();

  while (!result->IsFinished())
    dispatcher1->Process();

  EXPECT_TRUE(result->IsSuccessful());
  EXPECT_EQ(result->GetResult(), taskfw::kMainDispatcherID + 1);
}

TEST(DispatchTest, Dispatch_to_on_callback)
{
  auto dispatcher1 = std::make_shared<taskfw::ManualTaskDispatcher>();
  auto dispatcher2 = std::make_shared<taskfw::ThreadTaskDispatcher>(std::chrono::microseconds(0));

  taskfw::Dispatcher::instance().AddTaskDispatcher(taskfw::kMainDispatcherID, dispatcher1);
  taskfw::Dispatcher::instance().AddTaskDispatcher(taskfw::kMainDispatcherID + 1, dispatcher2);

  auto thread_id = std::this_thread::get_id();
  std::atomic<bool> ready(false);
  taskfw::dispatch_to_on_callback<SimpleTask, taskfw::kMainDispatcherID + 1>(
    [&ready, thread_id](SimpleTask::DeferredResult result) {
      EXPECT_TRUE(result->IsSuccessful());
      EXPECT_EQ(result->GetResult(), taskfw::kMainDispatcherID + 1);
      EXPECT_EQ(thread_id, std::this_thread::get_id());
      ready = true;
  });

  while (!ready)
    dispatcher1->Process();
}

TEST(DispatchTest, Dispatch_to_on_callback_from) {
  auto dispatcher1 = std::make_shared<taskfw::ManualTaskDispatcher>();
  auto dispatcher2 = std::make_shared<taskfw::ThreadTaskDispatcher>(std::chrono::microseconds(0));

  taskfw::Dispatcher::instance().AddTaskDispatcher(taskfw::kMainDispatcherID, dispatcher1);
  taskfw::Dispatcher::instance().AddTaskDispatcher(taskfw::kMainDispatcherID + 1, dispatcher2);

  auto thread_id = std::this_thread::get_id();
  std::atomic<bool> ready(false);
  taskfw::dispatch_to_on_callback_from<SimpleTask, taskfw::kMainDispatcherID + 1, taskfw::kMainDispatcherID>(
    [&ready, thread_id](SimpleTask::DeferredResult result) {
      EXPECT_TRUE(result->IsSuccessful());
      EXPECT_EQ(result->GetResult(), taskfw::kMainDispatcherID + 1);
      EXPECT_EQ(thread_id, std::this_thread::get_id());
      ready = true;
  });

  while (!ready)
    dispatcher1->Process();
}


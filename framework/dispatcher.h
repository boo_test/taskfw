#pragma once

#include "task_dispatcher_base.h"
#include "task.h"

#include <mutex>
#include <unordered_map>

namespace taskfw
{

class Dispatcher
{
public:
  static Dispatcher& instance()
  {
    static Dispatcher _instance;
    return _instance;
  }

  void AddTaskDispatcher(DispatcherID dispatcher_id, std::shared_ptr<TaskDispatcherBase> dispatcher)
  {
    std::unique_lock<std::mutex> lock(mutex);
    dispatchers[dispatcher_id] = std::move(dispatcher);
  }

  void RemoveTaskDispatcher(DispatcherID dispatcher_id)
  {
    std::unique_lock<std::mutex> lock(mutex);
    dispatchers.erase(dispatcher_id);
  }

  void Dispatch(DispatcherID dispatcher_id, TaskPtr task)
  {
    std::unique_lock<std::mutex> lock(mutex);

    internal::TaskContextGetter(task.get())->ChangeDispatcherID(dispatcher_id);
    dispatchers.at(dispatcher_id)->Dispatch(std::move(task));
  }

  void RemoveAll()
  {
    std::unique_lock<std::mutex> lock(mutex);

    for (auto& dispatcher : dispatchers)
    {
      dispatcher.second->Stop();
    }

    dispatchers.clear();
  }

private:
  std::mutex mutex;
  std::unordered_map<DispatcherID, std::shared_ptr<TaskDispatcherBase>> dispatchers;
};

}

namespace taskfw
{
// dispatch templates

// simple dispatch
template <typename TaskType>
typename TaskType::DeferredResult dispatch(const DispatcherID dispatcherId, typename std::shared_ptr<TaskType> task)
{
	Dispatcher::instance().Dispatch(dispatcherId, task);
	return typename TaskType::DeferredResult(task);
}

/* variadic dispatches

	// --- standard dispatch ---

	// dispatch to Main dispatcher
	dispatch<TaskName>(var1, var2, ...);

	// dispatch to selected dispatcher
	dispatch_to<TaskName, 2>(var1, var2, ...);

	// --- dispatch with result to callback ---

	// dispatch to Main dispatcher
	dispatch_on_callback<TaskName>(cbFunc, var1, var2, ...);

	// dispatch to selected dispatcher with calling callback from Main dispatcher
	dispatch_to_on_callback<TaskName, 2>(cbFunc, var1, var2, ...);

	// dispatch to Main dispatcher with calling callback from selected dispatcher
	dispatch_on_callback_from<TaskName, 2>(cbFunc, var1, var2, ...);

	// dispatch to selected dispatcher with calling callback from another dispatcher
	dispatch_to_on_callback_from<TaskName, 2, 3>(cbFunc, var1, var2, ...);
*/

template <typename TaskType, typename... Args>
DeferredResultWithTask<TaskType> dispatch(Args&&... args) 
{
  auto task = std::make_shared<TaskType>(std::forward<Args>(args)...);
  Dispatcher::instance().Dispatch(kMainDispatcherID, task);
  return DeferredResultWithTask<TaskType>(std::move(task));
}

template <typename TaskType, DispatcherID dispatcherId, typename... Args>
DeferredResultWithTask<TaskType> dispatch_to(Args&&... args) 
{
  auto task = std::make_shared<TaskType>(std::forward<Args>(args)...);
  Dispatcher::instance().Dispatch(dispatcherId, task);
  return DeferredResultWithTask<TaskType>(std::move(task));
}

template <typename TaskType, typename... Args>
typename std::shared_ptr<TaskType> dispatch_on_callback(typename TaskToCallback<TaskType>::CallbackFunc onFinish, Args&&... args) 
{
  auto task = std::make_shared<TaskType>(std::forward<Args>(args)...);
  auto taskOnCallback = std::make_shared<TaskToCallback<TaskType>>(task, onFinish);
  Dispatcher::instance().Dispatch(kMainDispatcherID, std::move(taskOnCallback));
  return task;
}

template <typename TaskType, DispatcherID dispatcherId, typename... Args>
typename std::shared_ptr<TaskType> dispatch_to_on_callback(typename TaskToCallback<TaskType>::CallbackFunc onFinish, Args&&... args) 
{
  auto task = std::make_shared<TaskType>(std::forward<Args>(args)...);
  internal::TaskContextGetter(task.get())->ChangeDispatcherID(dispatcherId);

  auto taskOnCallback = std::make_shared<TaskToCallback<TaskType>>(task, onFinish, kMainDispatcherID);
  Dispatcher::instance().Dispatch(dispatcherId, std::move(taskOnCallback));
  return task;
}

template <typename TaskType, DispatcherID callbackDispatcherId, typename... Args>
typename std::shared_ptr<TaskType> dispatch_on_callback_from(typename TaskToCallback<TaskType>::CallbackFunc onFinish, Args&&... args) 
{
  auto task = std::make_shared<TaskType>(std::forward<Args>(args)...);
  internal::TaskContextGetter(*task)->ChangeDispatcherID(kMainDispatcherID);

  auto taskOnCallback = std::make_shared<TaskToCallback<TaskType>>(task, onFinish, callbackDispatcherId);
  Dispatcher::instance().Dispatch(kMainDispatcherID, std::move(taskOnCallback));
  return task;
}

template <typename TaskType, DispatcherID dispatcherId, DispatcherID callbackDispatcherId, typename... Args>
typename std::shared_ptr<TaskType> dispatch_to_on_callback_from(typename TaskToCallback<TaskType>::CallbackFunc onFinish, Args&&... args) 
{
  auto task = std::make_shared<TaskType>(std::forward<Args>(args)...);
  internal::TaskContextGetter(task.get())->ChangeDispatcherID(dispatcherId);

  auto taskOnCallback = std::make_shared<TaskToCallback<TaskType>>(task, onFinish, callbackDispatcherId);
  Dispatcher::instance().Dispatch(dispatcherId, std::move(taskOnCallback));
  return task;
}


}


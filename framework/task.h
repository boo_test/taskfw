#pragma once

#include <functional>

#include "task_context_and_state.h"
#include "task_promise.h"
#include "task_promises.h"
#include "Macro.h"

namespace taskfw
{

class TaskCall : public virtual TaskState
{
public:
  virtual ~TaskCall() {}
  virtual void operator()() = 0;
};

template <typename TaskReturnType = TaskState>
class Task : public TaskCall, public virtual TaskReturnType
{
friend class internal::TaskContextGetter;
public:
  typedef const TaskReturnType ReturnType;
  typedef TaskPromiseTo<ReturnType> DeferredResult;

};

template <typename Task>
class DeferredResultWithTask : public Task::DeferredResult
{
public:
  DeferredResultWithTask(std::shared_ptr<Task> task)
    : Task::DeferredResult(task)
    , task(task)
  {
  }

  std::shared_ptr<Task> task;
};

using TaskPtr = std::shared_ptr<TaskCall>;

// helpers for frequently used types
template <typename ResultType, typename ErrorType = bool>
class TaskWithResult : public Task<ReturnResult<ResultType, ErrorType>> {};

template <typename ResultType, typename IntermediateType = float, typename ErrorType = bool>
class TaskWithResultAndIntermediate : public Task<ReturnResultWithIntermediate<ResultType, IntermediateType, ErrorType>> {};

// frequently used types
typedef TaskWithResult<bool> TaskWithBoolResult;

template <typename TaskType>
class TaskToCallback : public Task<>
{
public:
  template <class T> struct I { typedef T type; };
  typedef typename I<std::function<void (typename TaskType::DeferredResult)>>::type CallbackFunc;

  TaskToCallback(std::shared_ptr<TaskType> _task, CallbackFunc _on_finish, const DispatcherID _callback_dispatcher_id = kUndefinedDispatcherID)
    : task(_task)
    , task_context(_task.get())
    , result(_task)
    , on_finish(_on_finish)
    , callback_dispatcher_id(_callback_dispatcher_id)
  {
  }

  void operator()()
  {
    taskfw_task
    {
      do {
        task->operator()();
        if (task_context->NeedToChangeDispatcher())
        {
          task_context->ChangeDispatcherID(task_context->GetNextDispatcherID());
          taskfw_switch_dispatcher(task_context->GetCurrentDispatcherID());
        }
        else if (!task_context->IsInFinishedState())
        {
          taskfw_yield;
        }
      } while (!task_context->IsInFinishedState());
      
      if (callback_dispatcher_id != kUndefinedDispatcherID &&
          callback_dispatcher_id != taskfw_dispatcher)
      {
          taskfw_switch_dispatcher(callback_dispatcher_id);
      }

      on_finish(result);
      taskfw_finish;
    }
  }

private:
  const TaskPtr task;
  const internal::TaskContextGetter task_context;
  const typename TaskType::DeferredResult result;
  const CallbackFunc on_finish;
  const DispatcherID callback_dispatcher_id;
};

}

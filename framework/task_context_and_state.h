#pragma once

#include <memory>

namespace taskfw
{
using DispatcherID = int;
const DispatcherID kUndefinedDispatcherID = -1;
const DispatcherID kMainDispatcherID = 0;

namespace internal
{

class TaskContext
{
public:
  enum KnownStates
  {
    kFinished = -1,
    kStart = 0
  };

  TaskContext()
    : state(kStart)
    , curr_dispatcher_id(kUndefinedDispatcherID)
    , next_dispatcher_id(kUndefinedDispatcherID)
  {
  }

  // State controls.

  int GetState() const
  {
    return state;
  }

  bool IsInFinishedState() const
  {
    return GetState() == kFinished;
  }

  void SetState(int _state)
  {
    state = _state;
  }

  void Restart()
  {
    state = kStart;
  }

  void Finish()
  {
    state = kFinished;
  }

  // Dispatcher controls.

  DispatcherID GetCurrentDispatcherID() const
  {
    return curr_dispatcher_id;
  }

  DispatcherID GetNextDispatcherID() const
  {
    return next_dispatcher_id;
  }

  bool NeedToChangeDispatcher() const
  {
    return GetCurrentDispatcherID() != GetNextDispatcherID();
  }

  void ChangeDispatcherID(DispatcherID _dispatcher)
  {
    curr_dispatcher_id = next_dispatcher_id;
    next_dispatcher_id = _dispatcher;

    if (curr_dispatcher_id == kUndefinedDispatcherID)
      curr_dispatcher_id = _dispatcher;
  }

private:
  TaskContext(const TaskContext&) = delete;
  TaskContext(TaskContext&&) = delete;
  TaskContext& operator =(const TaskContext&) = delete;
  TaskContext& operator =(TaskContext&&) = delete;

  int state;
  DispatcherID curr_dispatcher_id;
  DispatcherID next_dispatcher_id;
};

class TaskContextGetter;

}  // namespace internal

class TaskState
{
public:
  bool IsFinished() const
  {
    return context.IsInFinishedState();
  }

protected:
  friend class internal::TaskContextGetter;
  internal::TaskContext context;
};

namespace internal
{

class TaskContextGetter
{
public:
  TaskContextGetter(TaskState* _task)
    : task_context(&_task->context)
  {
  }

  TaskContext* operator->() const
  {
    return task_context;
  }

  TaskContext& get() const
  {
    return *task_context;
  }

private:
  TaskContext* task_context;
};

}  // namespace internal

}  // namespace taskfw

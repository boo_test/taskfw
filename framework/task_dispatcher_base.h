#pragma once

#include "task.h"

namespace taskfw
{

class TaskDispatcherBase
{
public:
  virtual ~TaskDispatcherBase() {}

  virtual void Dispatch(TaskPtr task) = 0;
  virtual void Stop() = 0;
};

}  // namespace taskfw


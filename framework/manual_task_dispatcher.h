#pragma once

#include <list>
#include <mutex>

#include "dispatcher.h"

namespace taskfw
{

class ManualTaskDispatcher : public TaskDispatcherBase
{
public:
  ~ManualTaskDispatcher() override
  {
  }

  void Dispatch(TaskPtr task) override
  {
    std::unique_lock<std::mutex> lock(incoming_lock);
    incoming_tasks.push_back(std::move(task));
  }

  void Stop() override
  {
    std::unique_lock<std::mutex> lock1(inprogress_lock);
    std::unique_lock<std::mutex> lock2(incoming_lock);

    incoming_tasks.clear();
    inprogress_tasks.clear();
  }

  void Process()
  {
    ProcessIncoming();
    ProcessInProgress();
  }

protected:

  void ProcessIncoming()
  {
    std::unique_lock<std::mutex> lock(incoming_lock);
    inprogress_tasks.splice(inprogress_tasks.begin(), incoming_tasks);
  }

  void ProcessInProgress()
  {
    std::unique_lock<std::mutex> lock(inprogress_lock);

    auto iter  = inprogress_tasks.begin();
    auto eiter = inprogress_tasks.end();

    while (iter != eiter)
    {
      TaskPtr task = *iter;

      task->operator()();

      const auto& task_context = internal::TaskContextGetter(task.get());

      const bool is_finished = task_context->IsInFinishedState();
      const bool need_to_change_dispatcher = task_context->NeedToChangeDispatcher();

      if (is_finished || need_to_change_dispatcher)
      {
        iter = inprogress_tasks.erase(iter);

        if (need_to_change_dispatcher)
        {
          Dispatcher::instance().Dispatch(task_context->GetNextDispatcherID(), task);
        }
      }
      else
      {
        ++iter;
      }
    }
  }

  std::mutex         incoming_lock;
  std::list<TaskPtr> incoming_tasks;

  std::mutex         inprogress_lock;
  std::list<TaskPtr> inprogress_tasks;
};

}  // namespace taskfw


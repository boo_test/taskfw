#pragma once

#include "task.h"
#include "dispatcher.h"
#include "manual_task_dispatcher.h"
#include "thread_task_dispatcher.h"
#include "macro.h"

/**

** TASK EXAMPLES **

Task without return/error value:

struct MyTask : taskfw::Task<>
{
	int i;
	MyTask() : i() {}

	void operator()()
	{
		taskfw_task // ** task start block **
		{
			while (i < 5)
			{
				std::cout << ++i << " yielding" << std::endl;
				taskfw_yield; // ** task yield operation **
			}

			std::cout << ++i << " finishing" << std::endl;
			taskfw_finish; // ** task finish operation. use only if task doesn't return anything (derived from Task<>) **
		}
	}
};

Task with result/error return:

struct MyTask : taskfw::Task<taskfw::ReturnResult<int, std::string>>
{
	void operator()()
	{
		static int i = 10;
		taskfw_task // ** task start block **
		{
			std::cout << ++i << std::endl;
			taskfw_yield; // ** task yield operation **

			std::cout << ++i << std::endl;
			if (i == 2)
				taskfw_return_result(i);		// ** task result operation (includes taskfw_finish) **
			else 
				taskfw_return_error("error");	// ** task error operation (includes taskfw_finish) **
		}
	}
};

Task with return(/error) value and itermediate value:

struct MyTask : taskfw::Task<taskfw::ReturnResultWithIntermediate<int, float, std::string>>
{
	void operator()()
	{
		static int i = 10;
		taskfw_task // ** task start block **
		{
			std::cout << ++i << std::endl;
			taskfw_yield; // ** task yield operation **

			while (i < 10)
			{
				std::cout << ++i << std::endl;
				taskfw_set_intermediate(i * 0.1f); // ** setting intermediate value for task **
				taskfw_yield; // ** task yield operation **
			}

			if (i == 10)
				taskfw_return_result(i);		// ** task result operation (includes taskfw_finish) **
			else 
				taskfw_return_error("error");	// ** task error operation (includes taskfw_finish) **
		}
	}
};

*/
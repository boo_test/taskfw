#pragma once

#include <memory>
#include <type_traits>

namespace taskfw
{

template <typename ReturnType>
class TaskPromiseTo
{
  typedef typename std::enable_if<
    std::is_base_of<TaskState, ReturnType>::value, 
    typename std::shared_ptr<ReturnType>
  >::type TaskReturnPtr;

public:
  TaskPromiseTo() {}

  TaskPromiseTo(TaskReturnPtr _result)
    : result(_result)
  {
  }

  TaskPromiseTo(const TaskPromiseTo<ReturnType>& r)
    : result(r.result)
  {
  }

  TaskPromiseTo<ReturnType>& operator=(const TaskPromiseTo<ReturnType>& r)
  {
    result = r.result;
    return *this;
  }

  virtual ~TaskPromiseTo() {}

  ReturnType* operator->() const
  {
    return result.get();
  }

  void Reset()
  {
    result.reset();
  }

private:
  TaskReturnPtr result;
};

}
